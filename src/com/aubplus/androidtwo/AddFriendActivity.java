package com.aubplus.androidtwo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddFriendActivity extends Activity {

  // Defining UI Elements:
  private EditText emailEditText;
  private EditText firstNameEditText;
  private EditText ageEditText;
  private Button addFriendButton;

  private MySQLiteHelper mySQLiteHelper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_friend);

    // Instantiating a MySQLiteHelper.
    mySQLiteHelper = new MySQLiteHelper(this);

    // Linking:
    emailEditText = (EditText) findViewById(R.id.eET);
    firstNameEditText = (EditText) findViewById(R.id.fnET);
    ageEditText = (EditText) findViewById(R.id.aET);
    addFriendButton = (Button) findViewById(R.id.afButton);

    // Adding an OnClickListener:
    addFriendButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        String firstNameString = firstNameEditText.getText().toString();
        String emailString = emailEditText.getText().toString();
        String ageString = ageEditText.getText().toString();

        if (checkThatFieldsAreNotEmpty(firstNameString, emailString, ageString)) {
          Toast.makeText(getApplicationContext(), "One of the fields is empty", Toast.LENGTH_SHORT)
              .show();
          return;
        }

        mySQLiteHelper.addFriend(new Friend(firstNameString, emailString, Integer
            .parseInt(ageString)));

        finish();
      }
    });
  }

  @SuppressLint("NewApi")
  private boolean checkThatFieldsAreNotEmpty(String firstNameString, String emailString,
      String ageString) {
    return (firstNameString.isEmpty() || emailString.isEmpty() || ageString.isEmpty());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.add_friend, menu);
    return true;
  }
}
