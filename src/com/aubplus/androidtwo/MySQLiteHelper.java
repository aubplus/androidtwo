package com.aubplus.androidtwo;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLiteHelper extends SQLiteOpenHelper {

  private static final String DATABASE_NAME = "BigDatabase";

  private static final String TABLE_NAME = "Friends";
  private static final String KEY_EMAIL = "Email";
  private static final String KEY_FNAME = "FirstName";
  private static final String KEY_AGE = "Age";

  // Database creation SQL statement
  private static final String CREATE_TABLE_STRING = "CREATE TABLE " + TABLE_NAME + "(" + KEY_FNAME
      + " TEXT," + KEY_EMAIL + " TEXT," + KEY_AGE + " TEXT);";

  public MySQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, 1);
  }

  public MySQLiteHelper(Context context, String name, CursorFactory factory, int version) {
    super(context, name, factory, version);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE_STRING);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    onCreate(db);
  }

  public void addFriend(Friend friend) {
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues values = new ContentValues();
    values.put(KEY_FNAME, friend.getFirstName());
    values.put(KEY_AGE, "" + friend.getAge());
    values.put(KEY_EMAIL, friend.getEmail());
    // Inserting Row
    db.insert(TABLE_NAME, null, values);
    db.close(); // Closing database connection
  }

  public ArrayList<Friend> getAllFriends() {
    SQLiteDatabase db = this.getReadableDatabase();

    ArrayList<Friend> friendList = new ArrayList<Friend>();
    String mySQLQuery = "SELECT  * FROM " + TABLE_NAME;

    Cursor cursor = db.rawQuery(mySQLQuery, null);

    try {
      if (cursor.moveToFirst()) {
        do {
          Friend friend = new Friend();
          friend.setFirstName(cursor.getString(0));
          friend.setEmail(cursor.getString(1));
          friend.setAge(Integer.parseInt(cursor.getString(2)));
          friendList.add(friend);
        } while (cursor.moveToNext());
      }
      return friendList;
    } finally {
      cursor.close();
    }
  }
}
