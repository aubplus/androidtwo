package com.aubplus.androidtwo;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FriendsListActivity extends ListActivity {

  // ArrayList of Strings, which is a List that can be addressed like an Array (using indices).
  private ArrayList<String> friendsFirstNameArrayList = new ArrayList<String>();

  // ArrayList of Friends, which is a List that can be addressed like an Array (using indices).
  private ArrayList<Friend> friendsArrayList = new ArrayList<Friend>();

  // An Adapter object acts as a bridge between an AdapterView (like a ListView) and the underlying
  // data for that view. The Adapter provides access to the data items. The Adapter is also
  // responsible for making a View for each item in the data set.
  private ArrayAdapter<String> listViewArrayAdapter;

  // Creates the Database + Lets us define functions that make it easy to INSERT, SELECT or execute
  // any other SQL query statement.
  private MySQLiteHelper mySQLiteHelper;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Note that there is no 'setContentView' here. This Activity is not tied to an XML layout.

    // Defining the mySQLiteHelper.
    mySQLiteHelper = new MySQLiteHelper(this);

    // Defining the listViewArrayAdapter.
    // The second parameter 'android.R.layout.simple_list_item_1' is a system-defined layout for the
    // simplest possible list view row: a row made of only one String.
    listViewArrayAdapter =
        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
            friendsFirstNameArrayList);

    // Setting the Adapter of the ListView built in the ListActivity.
    setListAdapter(listViewArrayAdapter);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Creates the menu; this adds items to the action bar or to the Menu button.
    getMenuInflater().inflate(R.menu.friends_list, menu);
    return true;
  }

  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    Toast.makeText(this, friendsArrayList.get(position).getEmail(), Toast.LENGTH_SHORT).show();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Get ID of MenuItem.
    switch (item.getItemId()) {
      case R.id.action_add:
        // Start Add Friend Activity.
        Intent intent = new Intent(FriendsListActivity.this, AddFriendActivity.class);
        startActivity(intent);
        return true;
    }

    return super.onOptionsItemSelected(item);
  }

  // OnResume is called when the Activity just became visible and can be interacted with.
  @Override
  public void onResume() {
    super.onResume();
    populateListView();
  }


  private void populateListView() {
    // Geting all the Friends we have in the Friends table in our BigDatabase.
    ArrayList<Friend> friendList = mySQLiteHelper.getAllFriends();

    // Clearing the ArrayList to make sure we don't add elements we previously added before.
    friendsFirstNameArrayList.clear();
    friendsArrayList.clear();

    // Read this as follows: "for all Friends in the FriendList"
    for (Friend friend : friendList) {
      // Adding each Friend in our friendsFirstNameArrayList.
      friendsFirstNameArrayList.add(friend.getFirstName());
      friendsArrayList.add(friend);
    }

    // Notify the Adapter (which is the bridge between Data and ListView) that the data changed.
    listViewArrayAdapter.notifyDataSetChanged();
  }
}
